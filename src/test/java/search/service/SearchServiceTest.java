package search.service;

import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;

public class SearchServiceTest {
	
	@Autowired
	SearchService searchService;
		
	@Test
    public void getSearchResults() throws JsonProcessingException {
        
        assertTrue(StringUtils.isNotBlank(searchService.getSearchResults("veru")));
        
    }

}
