package search.model;

public class Results {
	
	public Results() {
	}
	
	private int count;
	
	private java.util.List<Items> items;

	public java.util.List<Items> getItems() {
		return items;
	}

	public void setItems(java.util.List<Items> items) {
		this.items = items;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
}
