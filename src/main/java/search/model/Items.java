package search.model;

import org.dozer.Mapping;

public final class Items {
	
	public Items() {
	}

 
  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String displayLink;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String formattedUrl;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String htmlFormattedUrl;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String htmlSnippet;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String htmlTitle;

  /**
   * The value may be {@code null}.
   */
  
  private Image image;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String linkToArticle;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String textSnippet;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String title;

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getDisplayLink() {
    return displayLink;
  }

  /**
   * @param displayLink displayLink or {@code null} for none
   */
  public Items setDisplayLink(java.lang.String displayLink) {
    this.displayLink = displayLink;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getFormattedUrl() {
    return formattedUrl;
  }

  /**
   * @param formattedUrl formattedUrl or {@code null} for none
   */
  public Items setFormattedUrl(java.lang.String formattedUrl) {
    this.formattedUrl = formattedUrl;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getHtmlFormattedUrl() {
    return htmlFormattedUrl;
  }

  /**
   * @param htmlFormattedUrl htmlFormattedUrl or {@code null} for none
   */
  public Items setHtmlFormattedUrl(java.lang.String htmlFormattedUrl) {
    this.htmlFormattedUrl = htmlFormattedUrl;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getHtmlSnippet() {
    return htmlSnippet;
  }

  /**
   * @param htmlSnippet htmlSnippet or {@code null} for none
   */
  public Items setHtmlSnippet(java.lang.String htmlSnippet) {
    this.htmlSnippet = htmlSnippet;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getHtmlTitle() {
    return htmlTitle;
  }

  /**
   * @param htmlTitle htmlTitle or {@code null} for none
   */
  public Items setHtmlTitle(java.lang.String htmlTitle) {
    this.htmlTitle = htmlTitle;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public Image getImage() {
    return image;
  }

  /**
   * @param image image or {@code null} for none
   */
  public Items setImage(Image image) {
    this.image = image;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getTitle() {
    return title;
  }

  /**
   * @param title title or {@code null} for none
   */
  public Items setTitle(java.lang.String title) {
    this.title = title;
    return this;
  }

	@Mapping("link")
	public java.lang.String getLinkToArticle() {
		return linkToArticle;
	}

public void setLinkToArticle(java.lang.String linkToArticle) {
	this.linkToArticle = linkToArticle;
}

@Mapping("snippet")
public java.lang.String getTextSnippet() {
	return textSnippet;
}

public void setTextSnippet(java.lang.String textSnippet) {
	this.textSnippet = textSnippet;
}
}
