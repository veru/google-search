package search.data;

public class Result {
	
	public Result() {
	}
	
	private java.util.List<Items> items;

	public java.util.List<Items> getItems() {
		return items;
	}

	public void setItems(java.util.List<Items> items) {
		this.items = items;
	}
	
}
