package search.data;

public final class Labels {

    /**
     * The value may be {@code null}.
     */
    
    private java.lang.String displayName;

    /**
     * The value may be {@code null}.
     */
    private java.lang.String labelWithOp;

    /**
     * The value may be {@code null}.
     */
    
    private java.lang.String name;

    /**
     * @return value or {@code null} for none
     */
    public java.lang.String getDisplayName() {
      return displayName;
    }

    /**
     * @param displayName displayName or {@code null} for none
     */
    public Labels setDisplayName(java.lang.String displayName) {
      this.displayName = displayName;
      return this;
    }

    /**
     * @return value or {@code null} for none
     */
    public java.lang.String getLabelWithOp() {
      return labelWithOp;
    }

    /**
     * @param labelWithOp labelWithOp or {@code null} for none
     */
    public Labels setLabelWithOp(java.lang.String labelWithOp) {
      this.labelWithOp = labelWithOp;
      return this;
    }

    /**
     * @return value or {@code null} for none
     */
    public java.lang.String getName() {
      return name;
    }

    /**
     * @param name name or {@code null} for none
     */
    public Labels setName(java.lang.String name) {
      this.name = name;
      return this;
    }

  }