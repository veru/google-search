package search.data;


public final class Items {
	
    public Items() {
	}

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String cacheId;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String displayLink;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String fileFormat;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String formattedUrl;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String htmlFormattedUrl;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String htmlSnippet;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String htmlTitle;

  /**
   * The value may be {@code null}.
   */
  
  private Image image;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String kind;

  /**
   * The value may be {@code null}.
   */
  
  private java.util.List<Labels> labels;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String link;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String mime;

  /**
   * The value may be {@code null}.
   */
  
  private java.util.Map<String, java.util.List<java.util.Map<String, java.lang.Object>>> pagemap;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String snippet;

  /**
   * The value may be {@code null}.
   */
  
  private java.lang.String title;

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getCacheId() {
    return cacheId;
  }

  /**
   * @param cacheId cacheId or {@code null} for none
   */
  public Items setCacheId(java.lang.String cacheId) {
    this.cacheId = cacheId;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getDisplayLink() {
    return displayLink;
  }

  /**
   * @param displayLink displayLink or {@code null} for none
   */
  public Items setDisplayLink(java.lang.String displayLink) {
    this.displayLink = displayLink;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getFileFormat() {
    return fileFormat;
  }

  /**
   * @param fileFormat fileFormat or {@code null} for none
   */
  public Items setFileFormat(java.lang.String fileFormat) {
    this.fileFormat = fileFormat;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getFormattedUrl() {
    return formattedUrl;
  }

  /**
   * @param formattedUrl formattedUrl or {@code null} for none
   */
  public Items setFormattedUrl(java.lang.String formattedUrl) {
    this.formattedUrl = formattedUrl;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getHtmlFormattedUrl() {
    return htmlFormattedUrl;
  }

  /**
   * @param htmlFormattedUrl htmlFormattedUrl or {@code null} for none
   */
  public Items setHtmlFormattedUrl(java.lang.String htmlFormattedUrl) {
    this.htmlFormattedUrl = htmlFormattedUrl;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getHtmlSnippet() {
    return htmlSnippet;
  }

  /**
   * @param htmlSnippet htmlSnippet or {@code null} for none
   */
  public Items setHtmlSnippet(java.lang.String htmlSnippet) {
    this.htmlSnippet = htmlSnippet;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getHtmlTitle() {
    return htmlTitle;
  }

  /**
   * @param htmlTitle htmlTitle or {@code null} for none
   */
  public Items setHtmlTitle(java.lang.String htmlTitle) {
    this.htmlTitle = htmlTitle;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public Image getImage() {
    return image;
  }

  /**
   * @param image image or {@code null} for none
   */
  public Items setImage(Image image) {
    this.image = image;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getKind() {
    return kind;
  }

  /**
   * @param kind kind or {@code null} for none
   */
  public Items setKind(java.lang.String kind) {
    this.kind = kind;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.util.List<Labels> getLabels() {
    return labels;
  }

  /**
   * @param labels labels or {@code null} for none
   */
  public Items setLabels(java.util.List<Labels> labels) {
    this.labels = labels;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getLink() {
    return link;
  }

  /**
   * @param link link or {@code null} for none
   */
  public Items setLink(java.lang.String link) {
    this.link = link;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getMime() {
    return mime;
  }

  /**
   * @param mime mime or {@code null} for none
   */
  public Items setMime(java.lang.String mime) {
    this.mime = mime;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.util.Map<String, java.util.List<java.util.Map<String, java.lang.Object>>> getPagemap() {
    return pagemap;
  }

  /**
   * @param pagemap pagemap or {@code null} for none
   */
  public Items setPagemap(java.util.Map<String, java.util.List<java.util.Map<String, java.lang.Object>>> pagemap) {
    this.pagemap = pagemap;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getSnippet() {
    return snippet;
  }

  /**
   * @param snippet snippet or {@code null} for none
   */
  public Items setSnippet(java.lang.String snippet) {
    this.snippet = snippet;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getTitle() {
    return title;
  }

  /**
   * @param title title or {@code null} for none
   */
  public Items setTitle(java.lang.String title) {
    this.title = title;
    return this;
  }

  /**
   * Model definition for ResultImage.
   */


  /**
   * Model definition for ResultLabels.
   */


}
