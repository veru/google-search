package search.data;

public final class Image {
	
    public Image() {
	}

    /**
     * The value may be {@code null}.
     */
    
    private java.lang.Integer byteSize;



	/**
     * The value may be {@code null}.
     */
    
    private java.lang.String contextLink;

    /**
     * The value may be {@code null}.
     */
    
    private java.lang.Integer height;

    /**
     * The value may be {@code null}.
     */
    
    private java.lang.Integer thumbnailHeight;

    /**
     * The value may be {@code null}.
     */
    
    private java.lang.String thumbnailLink;

    /**
     * The value may be {@code null}.
     */
    
    private java.lang.Integer thumbnailWidth;

    /**
     * The value may be {@code null}.
     */
    
    private java.lang.Integer width;

    /**
     * @return value or {@code null} for none
     */
    public java.lang.Integer getByteSize() {
      return byteSize;
    }

    /**
     * @param byteSize byteSize or {@code null} for none
     */
    public Image setByteSize(java.lang.Integer byteSize) {
      this.byteSize = byteSize;
      return this;
    }

    /**
     * @return value or {@code null} for none
     */
    public java.lang.String getContextLink() {
      return contextLink;
    }

    /**
     * @param contextLink contextLink or {@code null} for none
     */
    public Image setContextLink(java.lang.String contextLink) {
      this.contextLink = contextLink;
      return this;
    }

    /**
     * @return value or {@code null} for none
     */
    public java.lang.Integer getHeight() {
      return height;
    }

    /**
     * @param height height or {@code null} for none
     */
    public Image setHeight(java.lang.Integer height) {
      this.height = height;
      return this;
    }

    /**
     * @return value or {@code null} for none
     */
    public java.lang.Integer getThumbnailHeight() {
      return thumbnailHeight;
    }

    /**
     * @param thumbnailHeight thumbnailHeight or {@code null} for none
     */
    public Image setThumbnailHeight(java.lang.Integer thumbnailHeight) {
      this.thumbnailHeight = thumbnailHeight;
      return this;
    }

    /**
     * @return value or {@code null} for none
     */
    public java.lang.String getThumbnailLink() {
      return thumbnailLink;
    }

    /**
     * @param thumbnailLink thumbnailLink or {@code null} for none
     */
    public Image setThumbnailLink(java.lang.String thumbnailLink) {
      this.thumbnailLink = thumbnailLink;
      return this;
    }

    /**
     * @return value or {@code null} for none
     */
    public java.lang.Integer getThumbnailWidth() {
      return thumbnailWidth;
    }

    /**
     * @param thumbnailWidth thumbnailWidth or {@code null} for none
     */
    public Image setThumbnailWidth(java.lang.Integer thumbnailWidth) {
      this.thumbnailWidth = thumbnailWidth;
      return this;
    }

    /**
     * @return value or {@code null} for none
     */
    public java.lang.Integer getWidth() {
      return width;
    }

    /**
     * @param width width or {@code null} for none
     */
    public Image setWidth(java.lang.Integer width) {
      this.width = width;
      return this;
    }

  }