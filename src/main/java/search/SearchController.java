package search;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import search.service.SearchService;

@RestController
public class SearchController {
	
	@Autowired
	SearchService searchService;
	
	@GetMapping("/search")
	@ResponseBody
	public String search(@RequestParam String q) throws JsonProcessingException {		
		String r = searchService.getSearchResults(q);
		return r;
	}

}
