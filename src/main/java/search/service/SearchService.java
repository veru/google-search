package search.service;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface SearchService {
	public String getSearchResults(String searchTerm) throws JsonProcessingException; 
}
