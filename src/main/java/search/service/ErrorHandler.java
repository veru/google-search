package search.service;

import java.io.IOException;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import search.util.RestUtil;

public class ErrorHandler implements ResponseErrorHandler{

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		
		System.out.println("http status code is " + response.getStatusCode());
		System.out.println("http status text is " + response.getStatusText());
		return RestUtil.isError(response.getStatusCode());
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		System.out.println("http status code is " + response.getStatusCode());
		System.out.println("http status text is " + response.getStatusText());
		
	}

}
