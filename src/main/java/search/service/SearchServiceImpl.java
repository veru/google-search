package search.service;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import search.data.Result;
import search.model.Results;

@Service("searchService")
public class SearchServiceImpl implements SearchService {
	
	@Autowired
	DozerBeanMapper mapper;
	
	@Autowired
	ObjectMapper oMapper;

	RestTemplate restTemplate = new RestTemplate();

	public String getSearchResults(String searchTerm) throws JsonProcessingException {
		this.restTemplate.setErrorHandler(new ErrorHandler());
		
		//get response and convert JSON to Java
		Result result = this.restTemplate.getForObject("https://www.googleapis.com/customsearch/v1?key=AIzaSyAory-1HthcT-RX0RvtWSWABd2jhS5YlT0&cx=017576662512468239146:omuauf_lfve&q={searchTerm}", Result.class, searchTerm);
		
		//convert Service Java to UI Model
		Results r = mapper.map(result, Results.class);
		
		//convert Java to JSON
		ObjectWriter ow = oMapper.writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(r);
		
		System.out.println(r.getItems().get(0).getTitle());
		return json;
	}

}
